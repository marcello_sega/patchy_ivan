
# Write the structure information of the current system to the given file
# OPTIONS:
#   short|verbose [verbose]
#   radius [{}]
#   typedesc [{}]
proc writevsf2 { file args } {
    proc flatten_indexed_list { n default_value indexed_list } {
	set flatlist {}
	# create a list of length n
	for { set i 0 } { $i < $n } { incr i } {
	    set flatlist [lappend flatlist $default_value]
	}

	foreach {index value} $indexed_list {
	    lset flatlist $index $value
	}

	return $flatlist
    }

    proc get_radius_list { type_max i_list } {
	set list [flatten_indexed_list [expr $type_max + 1] "auto" $i_list]
	for { set type 0 } { $type < [llength $list] } { incr type } {
	    if { [lindex $list $type] eq "auto" } then {
		set l [split [inter $type $type]]
		if {[lindex $l 2] eq "lennard-jones" } then {
		    # guess the radius from the self-lj-interaction
		    set lj_sigma [lindex $l 4]
		    set lj_offset [lindex $l 7]
		    set r [expr 0.5*($lj_sigma+$lj_offset)]
		    lset list $type $r
		} else {
		    # default radius is 0.5
		    lset list $type 0.5
		}
	    }
	}
	return $list
    }

    proc get_typedesc_list { type_max i_list } {
	set name_list { "O" "N" "S" "H" "C" "Z" }
	set list [flatten_indexed_list [expr $type_max + 1] "default" $i_list]
	for { set type 0 } { $type < [llength $list] } { incr type } {
	    if { [lindex $list $type] eq "default" } then {
		if { $type < [llength $name_list] } then {
		    lset list $type "name [lindex $name_list $type] type $type"
		} else {
		    lset list $type "name X type $type"
		}
	    }
	}
	return $list
    }

    # returns an atom record for the atoms $from to $to of type $type
    proc get_atom_record { from to type } {
	upvar 1 radiuslist radiuslist typedesclist typedesclist
	set desc "radius [lindex $radiuslist $type] [lindex $typedesclist $type]"
	if { $to == $from } then { 
	    return "atom [vtfpid $to] $desc" 
	} else {
	    return "atom [vtfpid $from]:[vtfpid $to] $desc"
	}
    }

    ##################################################
    # PROCEDURE CODE
    ##################################################
    set typedesc {}
    set radius {}
    set short 0

    # Parse options
    for { set argnum 0 } { $argnum < [llength $args] } { incr argnum } {
	set arg [ lindex $args $argnum ]
	set val [ lindex $args [expr $argnum+1]]
	switch -- $arg {
	    "typedesc" { 
		set typedesc $val
		incr argnum 
	    }
	    "radius" { 
		set radius $val
		incr argnum 
	    }
	    "verbose" { set short 0 }
	    "short" { set short 1 }
	    default { 
		puts "unknown option to writevsf: $arg"
		return 
	    }
	}
    }

    # cleanup particle mapping
    global vtf_pid
    array unset vtf_pid

    # Print unitcell data
    if { $short } then { 
	puts $file "p [setmd box_l]"
    } else { 
	puts $file "pbc [setmd box_l]"
    }

    set max_pid [setmd max_part]

    # compute the maximal type
    set max_type [setmd n_part_types]
    foreach {i j} $radius {
	if { $i > $max_type } then { set max_type $i }
    }
    foreach {i j} $typedesc {
	if { $i > $max_type } then { set max_type $i }
    }

    set typedesclist [get_typedesc_list $max_type $typedesc]
    set radiuslist [get_radius_list $max_type $radius]

    # collect combinations of charge and type
    set uses_electrostatics [has_feature "ELECTROSTATICS"]
    set qs ""
    set pid_to_desc [list]
    for { set pid 0 } { $pid <= $max_pid } { incr pid } {
        set type [part $pid print type]
        if { $uses_electrostatics } then { 
            set q [part $pid print q]
            set qs "q$q"
        }
        set this "t$type$qs"
            set combination($this) $pid
            set desc "radius [lindex $radiuslist $type]"
            set desc "$desc [lindex $typedesclist $type]"
            if { $uses_electrostatics } then { set desc "$desc q $q" }
            set combination("desc-$this") $desc
	lappend pid_to_desc $desc
    }
    if { $max_pid > 0 } {  
   	set prev_desc [lindex $pid_to_desc 0 ]
   	set prev_pid  0
    	for { set pid 1 } { $pid <= $max_pid } { incr pid } {
		if { [lindex $pid_to_desc $pid ] != $prev_desc || $pid == $max_pid} { 
			if { $pid == $max_pid } { 
				set lastpid $max_pid
			} else { 
				set lastpid [expr $pid -1]
			}
			puts $file "atom $prev_pid:$lastpid $prev_desc"		
			set prev_pid $pid
			set prev_desc [lindex $pid_to_desc $pid ]
		}
   	}
    }

    # Print bond data
    for { set from 0 } { $from <= $max_pid } { incr from } {
	if { [part $from] != "na" } then {
	    set bonds [lindex [part $from print bond] 0]
	    for { set i 0 } { $i < [llength $bonds] } { incr i } {
		set to [lindex $bonds $i 1]
		
		if { $short } then {
		    puts $file "b [vtfpid $from]:[vtfpid $to]"
		} else {
		    puts $file "bond [vtfpid $from]:[vtfpid $to]"
		}
	    }
	}
    }

    if { ! $short } then { puts $file "" }
}


