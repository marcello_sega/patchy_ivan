set niter 100000
set niter_steep 3
set hydrodynamics 1

# FILESYSTEM PATAMETERS
# where the sequence.dat file  is stored
set basedir "./"

# SIMULATION PARAMETERS
set box 120
setmd box_l $box $box $box 
set boxh [expr $box/2.]
setmd time_step 0.001
set plen 28
# interaction params
set sigma 1 
set Radius 1.0
set eps 3.1
set harmonic 0
set fene 1 
set K 500.
set Kf 10.0
set df [expr .5*$sigma]
# particle types
set patch_h 0
set patch_i 1
set patch_fake 2
set id_offset 10
#
