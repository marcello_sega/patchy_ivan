source "md_procedures.tcl"
source "params.tcl"
source "vtf.tcl"

# RUN LENGTH PARAMETERS
setmd warning 0
setmd min_global_cut [expr 5*$sigma]
setmd skin 0.2
thermostat off

#read the target "contact" map
set elements [calc_cm_from_file refCM "CA.xyz"]
#setup interactions
set_interactions "pot.dat"

# initialize the polymer
set fp [open "${basedir}/sequence.dat" "r"]
set index 0
while { [gets $fp line ] >= 0 } { 
    bead [setmd n_part] [expr $id_offset+$line]  $index
    incr index
}
close $fp

# notes:
# 1) the parameters for the thermostat/LB which are here are *not* those from our notes.
if { $hydrodynamics == 1 } { 
# 2) the density of the fluid is a critical parameter, as it sets the amplitude
#    of fluctuations! We should think about it a bit...
lbfluid cpu agrid 10 dens 10.0 visc 1.0 tau  0.1 friction 1.0
thermostat lb 1.0
} else {
# 3) here, to match the two friction we have to put in the (in)famous factor of Duenweg
thermostat langevin 1.0 1.0
}
integrate 0

set traj [open "/tmp/traj.vtf" "w"]
set typedesc [generate_vsf_typedesc  $id_offset ]
writevsf2 $traj typedesc  $typedesc 
writevcf $traj folded
exit
#simulate a steepest descent
set steepest_step 0.01
setmd time_step $steepest_step
steepest_descent $niter_steep id_offset refCM

puts "Energy minimization done. Starting MD."

set avt 0
for { set i 1 } { $i <= $niter } { incr i } { 
        set t [clock clicks -milliseconds]
	integrate 100
        writevcf $traj folded
        set dt [expr [clock clicks -milliseconds] - $t]
        set avt [expr $avt + $dt]
        if { [expr $i % 10 ] == 0 } { 
             puts "step $i : [expr [analyze energy total ] - [analyze energy kinetic] ] RMSD = [cm_rmsd refCM $id_offset ] ( $dt ms ; estimated time to end: [expr ($niter - $i)* $avt / $i / 1000 /60 ] min )"
        }
}
  


