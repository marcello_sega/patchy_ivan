/* global features */
#define PARTIAL_PERIODIC
#define EXTERNAL_FORCES
#define CONSTRAINTS
/* potentials */
#define ELECTROSTATICS
#define ROTATION
#define LENNARD_JONES
#define ROTATIONAL_INERTIA
#define MASS
#define EXCLUSIONS
#define VIRTUAL_SITES_RELATIVE
#define LB
#define LB_BOUNDARIES
#define TABULATED
#define LENNARD_JONES_GENERIC
#define LJCOS
#define LJ_ANGLE
#define GAY_BERNE
#define MORSE
#define SOFT_SPHERE
#define BOND_ANGLE
#define SMOOTH_STEP


