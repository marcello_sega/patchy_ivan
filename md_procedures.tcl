proc lexpr {a op b} {
     set res {}
     set la [llength $a]
     set lb [llength $b]
     if {$la == 1 && $lb == 1} {
         set res [expr $a $op $b]
     } elseif {$la==1} {
         foreach j $b {lappend res [lexpr $a $op $j]}
     } elseif {$lb==1} {
         foreach i $a {lappend res [lexpr $i $op $b]}
     } elseif {$la == $lb} {
         foreach i $a j $b {lappend res [lexpr $i $op $j]}
     } else {error "list length mismatch $la/$lb"}
     set res
 }


proc bead { startid colloid_type index } {
global Radius ;global sigma;global patch_h;global patch_i;global patch_fake;global boxh;global harmonic;global fene;global Kf;global df
    set n1 $startid
    set n2 [expr $n1 + 1 ]
    set n3 [expr $n1 + 2 ]
    set n4 [expr $n1 + 3 ]
    set n5 [expr $n1 + 4 ]
    part $n1  pos $boxh $boxh                   [ expr 1.1*$Radius + 2.* $index * ($sigma+$sigma/100.) ]                                          type $colloid_type
    part $n2  pos $boxh $boxh                   [ expr 1.1*$Radius + 2.* $index * ($sigma+$sigma/100.) + $Radius ] virtual 1  vs_auto_relate $n1  type $patch_h   
    part $n3  pos $boxh $boxh                   [ expr 1.1*$Radius + 2.* $index * ($sigma+$sigma/100.) - $Radius ] virtual 1  vs_auto_relate $n1  type $patch_h   
    part $n4  pos $boxh [expr $boxh + $Radius ] [ expr 1.1*$Radius + 2.* $index * ($sigma+$sigma/100.) ]           virtual 1  vs_auto_relate $n1  type $patch_i   
    part $n5  pos $boxh [expr $boxh - $Radius ] [ expr 1.1*$Radius + 2.* $index * ($sigma+$sigma/100.) ]           virtual 1  vs_auto_relate $n1  type $patch_fake
    if { $index > 0 } { 
	part $n3 bond $harmonic [expr $n3 - 6 ] 
#	part $n3 bond $fene [expr $n3 - 6 ] 
    }
}

proc set_interactions { filename } { 
   global harmonic ;global K;global sigma ;global eps;global fene;global Kf ;global df;global patch_i;global patch_h ;global id_offset ; global basedir
   inter forcecap individual  ; #capping is needed for lj-angle!
   inter $harmonic harmonic $K 1e-3 [expr 20*$sigma]
   inter $fene fene $Kf $df
   #inter $colloid $colloid lennard-jones $eps $sigma [expr 1.122462*$sigma]
   #inter $patch_i $patch_i lj-angle $eps $sigma [expr 5*$sigma] -1 -2  -1 -2 [expr 0.7*$sigma] 
   inter $patch_i $patch_i lj-angle $eps $sigma [expr 5*$sigma] -1 -2  -1 -2  
   inter $patch_i $patch_i lennard-jones $eps [expr $sigma] [expr 1.12246204830937*$sigma] 
   
   set fp [open "${basedir}/${filename}" r]
   set file_data [read $fp]
   close $fp
   set data [split $file_data "\n"]
   set i 0 ; set j 0
   foreach line $data { 
         inter  [expr $i+$id_offset ] [expr $j  + $id_offset]  smooth-step 0.0 1 $line 1.25 6.0 10.0
         incr j
         if { $j == 20 } {
   	 incr i
            if { $i == 20} {
                break
            }
            set j $i
         }
   }
}


proc generate_vsf_typedesc { offset } {
	set b [list 0 {name H} 1 {name I} 2 {name F}  ]
	
	for { set i 0 } { $i < [setmd n_part] } { incr i } { 
		set type [expr [part $i print type] ]
		if { [lsearch $b $type] == -1 } {
			set type2 [expr $type - $offset ]
			set name  "name C_$type2"
			#set name  "name D"
			lappend b $type
			lappend b $name
		}
	}
	return $b
}


proc calc_cm { cm data } { 
   upvar $cm localcm
   set data2 $data
   set i 0 
   set j 0 
   set max [expr [llength $data]-1]
   foreach li $data {
           set j 0 
   	foreach lj $data2 { 
                   set dv  [lexpr $li - $lj ]  
                   set d 0; foreach di $dv { set d [expr $d+$di*$di ] }
                   set a($i,$j) [expr sqrt($d)]
   		incr j
                   if { $j == $max } break
           }
           incr i
           if { $i == $max } break
   }
   set avbond 0
   for { set i 1 } { $i < $max } { incr i } {
      set avbond [expr $avbond+$a($i,[expr $i-1])]
   }
   
   for { set i 0 } { $i < $max } { incr i } {
     for { set j 0 } { $j < $max } { incr j } {
         set localcm($i,$j) [expr $a($i,$j)/$avbond * $max]
     }
   }
   return $max

  
}

proc calc_cm_from_file { cm datafile} { 
   upvar $cm localcm
   set fp [open $datafile "r"]
   set file_data [read $fp]
   set data [split $file_data "\n"]
   close $fp
   set elem [calc_cm  localcm $data ] 
   return $elem
}

proc calc_cm_from_positions { cm backbonetype} { 
   upvar $cm localcm
   set data {}
   for { set i 0 } { $i < [setmd n_part] } { incr i } { 
	if { [part $i print type ] >= $backbonetype } {
		lappend data [part $i print pos]
        }
   }
   return [calc_cm  localcm $data ] 
}

proc diff_cm { refCM backbonetype diffCM} { 
   upvar $diffCM localdiff
   upvar $refCM localref
   set elem [calc_cm_from_positions cm $backbonetype]
   for { set i 0 } { $i < $elem } { incr i } { 
     for { set j 0 } { $j < $elem } { incr j } { 
       set d [expr $localref($i,$j) - $cm($i,$j)]
       set localdiff($i,$j) [expr sqrt($d*$d)]
     }
   } 
   return $elem
}

proc cm_threshold { refCM backbonetype threshold} { 
   set elem [diff_cm $refCM $backbonetype diffCM]
   for { set i 0 } { $i < $elem } { incr i } { 
     for { set j 0 } { $j < $elem } { incr j } { 
 	if { $diffCM($i,$j) > $threshold } return 0
     }
   } 
   return 1 
}

proc cm_rmsd { refCM backbonetype } { 
   upvar $refCM localcm
   set rmsd 0 
   set elem [diff_cm localcm $backbonetype diffCM]
   for { set i 0 } { $i < $elem } { incr i } { 
     for { set j 0 } { $j < $elem } { incr j } { 
        set rmsd [ expr $rmsd +  $diffCM($i,$j)]
     }
   } 
   return [expr $rmsd / $elem ]  
}


proc steepest_descent { niter_steep backboneid refCM } {
   upvar $backboneid id_offset
   upvar $refCM locrefCM
   puts "Starting energy minimization ( $niter_steep iterations of 100 timesteps each ) "
   for { set i 0 } { $i < $niter_steep } { incr i } { 
              for { set j 0 } { $j < 100 } { incr j } { 
                   kill_particle_motion rotation
                   kill_particle_forces torques
                   integrate 20 
                   if { [expr $j % 100 ] == 0 } {
                   puts "minimzation step $i : [expr [analyze energy total ] - [analyze energy kinetic] ] RMSD = [cm_rmsd locrefCM $id_offset ] "
             #           writevcf $traj folded
                      }
                   }
   }
}


